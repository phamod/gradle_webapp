/**
 * 
 */

require.config({
	paths: {
		jquery: '../node_modules/jquery/dist/jquery.min',
		vue: '../node_modules/vue/dist/vue.min',
		'vue-router': '../node_modules/vue-router/dist/vue-router.min'
	}

})

require(['jquery', 'vue', 'vue-router'], function( $, Vue,  VueRouter) {
    
	Vue.use(VueRouter);
	var app = Vue.component('myapp', {
		methods: {
			drag: function(event){
				
				console.log(event);
				var target = {
						id: event.target.id,
						x: event.layerX,
						y: event.layerY
				}
				console.log(target)
				event.dataTransfer.setData("app", JSON.stringify(target));
			},
			location: function(event){
				console.log(event)
			}
		},
		template: `
		<div class="app" draggable="true"  v-on:dragstart="drag" v-on:click="location">
		   <div class="menubar">
			This is my app
		   </div>
		</div>
		`
	})
	
	
	var table = Vue.component('mytable', {
		  props: ['title'],
		  data: function(){
			  var data= {
					  sorter: '',
					  dragTarget: ''
			  }
		  },
		  methods:{
			sort: function(event){
				var sortKey = event.target.innerText
				
				if(sortKey == this.sorter){
					this.title.reverse();
				}
				else{
					this.title.sort(function(a, b){
						return (a[sortKey] < b[sortKey]) - (a[sortKey]> b[sortKey]);
					});
					this.sorter = sortKey;
				}
			},
			allowDrop: function(ev) {
			    ev.preventDefault();
			},

			drag: function(ev) {
			    ev.dataTransfer.setData("text", ev.target.innerText);
			    this.dragTarget = ev.target.innerText;
			    
			},

			drop: function(ev) {
			    ev.preventDefault();
			    console.log(ev);
			    var data = ev.dataTransfer.getData("text");
			    this.insertCol(data, ev.target.innerText);
			},
			insertCol(move, target){
				for(var i=0; i<this.title.length; i++){
					var obj ={};
					var row = this.title[i];
					for(col in row){
						if(col == target){
							obj[move] = row[move];
							obj[col] = row[col];
						}
						else if(col != move){
							obj[col] = row[col];
						}
					}
					
					Vue.set(this.title, i, obj);
				}
			}
		  },
		  template: `<table class="table table-bordered">
		   <tr>
    	       <th v-for="(col, header) in title[0]" v-on:click="sort"
    	       v-on:dragstart="drag" v-on:dragover="allowDrop" v-on:drop="drop"
    	        draggable="true">{{ header }}</th>
    	   </tr>
    	   <tr v-for="row in title">
    	      <td v-for="col in row">{{ col }}</td>
    	   </tr>
		  </table>
		  `
		})
	
    
    const Foo = { template: '<div>foo</div>' }
    
    const routes = [
//    	{path: '/test', component:Foo}
    	{path: '/test/', component:table, props: true},
    	{path: '/app', component:app, props: true}
    	
    ]
    
    const router = new VueRouter({
    	routes:routes
    })
    
    var app = new Vue({
    	el: '#app',
    	router,
    	data: function() {
    		var data = {
	    		buttons : [],
	    		table: [{test:"test", attr:"attr"}],
	    		message : "hello patrick"
    		}
    		return data
    	},
    	created: function(){
    		  var xhttp = new XMLHttpRequest();
    		  xhttp.onreadystatechange = function() {
    		    if (this.readyState == 4 && this.status == 200) {
    		      //app.buttons = this.responseText;
    		    	app.table = JSON.parse(this.responseText);
    		    	console.log(JSON.parse(this.responseText));
    		    	//var test = JSON.parse(this.responseText.events);

    		      //console.log(test);
    		    }
    		  };
    		  xhttp.open("GET", "http://localhost:8080/webapp/resources/rest/table", true);
    		  xhttp.setRequestHeader("Content-type", "application/json");
    		  xhttp.send();
    	},
    	methods: {
    		test: function(){
    			alert('this is a test');
    		},
    		drop: function(event){
    			event.preventDefault();
    			var data = event.dataTransfer.getData("app");
    			data = JSON.parse(data);
    			var app = document.getElementById(data.id);
    			console.log(data);
    			app.style["top"] = (event.clientY- data.y)+"px";
    			app.style["left"] = (event.clientX - data.x)+"px";
    			console.log(app);
    			//console.log(data.style['border']);
    		},
    		allowDrop: function(event){
    			event.preventDefault();
    		}
    	}
    })
});