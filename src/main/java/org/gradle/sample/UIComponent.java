package org.gradle.sample;

import java.util.HashMap;

public class UIComponent {
	public HashMap<String, String> attributes;
	public HashMap<String, String> events;
	public String url;
	public String type;
	public String service;
	public String request;
	
	public UIComponent()
	{
		attributes = new HashMap<String, String>();
		events = new HashMap<String, String>();
	}

}
