package org.gradle.sample;

public class TableData {
	
	public String attribute1;
	public String attribute2;
	public String attribute3;
	
	public TableData() {
		attribute1 = "attribute 1";
		attribute2 = "attribute 2";
		attribute3 = "attribute 3";
	}

}
