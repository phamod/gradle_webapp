package org.gradle.sample;

import java.io.OutputStream;
import java.io.Writer;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import java.io.IOException;
import java.awt.GraphicsDevice;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Random;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.StreamingOutput;




@Path("/rest")
public class Hello {

  @GET
  @Path("/hello")
  public String helloworld() {
      return "Hello World!";
  }
  
  @GET
  @Path("/ui")
  @Produces("application/json")
//  public ArrayList<UIComponent> getButtons()
  public UIComponent getButtons()
  {
	  ArrayList<UIComponent> buttons = new ArrayList<UIComponent>();
	  UIComponent button = new UIComponent();
	  button.events.put("click", "app.test()");
	  button.request = "POST";
	  button.service = "REST";
	  button.type = "text/xml";
	  button.url = "http://localhost:8080/webapp/resources/rest/hello";
	  
	  buttons.add(button);
	  return button;
  }
  
  @GET
  @Path("/table")
  @Produces("application/json")
//  public ArrayList<UIComponent> getButtons()
  public ArrayList<TableData> getTable()
  {
	  ArrayList<TableData> table = new ArrayList<TableData>();
	  TableData data = new TableData();
	  data.attribute1 = "Patrick";
	  data.attribute2 = "Francis";
	  data.attribute3 = "Hamod";
	  table.add(data);
	  
	  for(int i=0; i<10; i++)
	  {
		  data = new TableData();
		  table.add(data);
	  }
	  
	  return table;
  }
  
//  @GET
//  @Path("/test")
//  @Produces(MediaType.TEXT_PLAIN)
//  public StreamingOutput streamExample() {
//    StreamingOutput stream = new StreamingOutput() {
//  	  
//      public void write(OutputStream os) throws IOException,
//      WebApplicationException {
//        Writer writer = new BufferedWriter(new OutputStreamWriter(os));
//        writer.write("test");
//        writer.flush();  // <-- This is very important.  Do not forget.
//      }
//    };
//    return stream;
//  }
  
//  @GET
//  @Path("/test")
//  @Produces("blob")
//  public StreamingOutput streamExample() {
//	  GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
//	  int width = gd.getDisplayMode().getWidth();
//	  int height = gd.getDisplayMode().getHeight();
//	  
//	  
//  }
}


//import javax.ws.rs.GET;  
//import javax.ws.rs.Path;
//import javax.ws.rs.PathParam;
//import javax.ws.rs.Produces;  
//import javax.ws.rs.core.MediaType;
//import javax.ws.rs.core.Response;  
//
//@Path("rest/hello")  
//public class Hello {  
//  // This method is called if HTML and XML is not requested  
////  @GET  
////  @Produces(MediaType.TEXT_PLAIN)  
////  public String sayPlainTextHello() {  
////    return "Hello Jersey Plain";  
////  }  
////  // This method is called if XML is requested  
////  @GET  
////  @Produces(MediaType.TEXT_XML)  
////  public String sayXMLHello() {  
////    return "<?xml version=\"1.0\"?>" + "<hello> Hello Jersey" + "</hello>";  
////  }  
////  
//  // This method is called if HTML is requested  
//  @GET  
//  @Produces("text/xml")  
//  public String sayHtmlHello() { 
//	    String output = "<html> " + "<title>" + "Hello Jersey" + "</title>"  
//	            + "<body><h1>" + "Hello Jersey HTML" + "</h1></body>" + "</html> ";	  
//	  return output;
//  
//  }  
//  
////  @GET
////  @Path("/{param}")
////  public String getMessage(@PathParam("param") String message) {
////   String output = "Jersey say Hello World!!! : " + message;
////   return Response.status(200).entity(output).build();
////  }
//}   